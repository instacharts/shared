package com.instacharts.shared.dto;

public class SnapshotDto {
    private String accessToken;

    private String snapshootId;

    SnapshotDto(String accessToken, String snapshootId) {
        this.accessToken = accessToken;
        this.snapshootId = snapshootId;
    }
}
