package com.instacharts.shared.entity;

import org.springframework.data.annotation.Id;

import java.util.Date;

public class Snapshot {
    @Id
    private String id;

    private Date date;

    private String user;
}
